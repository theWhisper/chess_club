// Now that you are connected, you can join channels with a topic:
import socket from "./socket"

let globalFeedBox = document.getElementById('global-notifications')
//globalFeedBox.scrollIntoView({block: "end", inline: "nearest"});

let GlobalNotifications ={
    init(socket){
        let channel = socket.channel('gnotifications', {})
        channel.join()
          .receive("ok", resp =>  console.log(`You will receive notifications now.`))
          .receive("error", resp => { console.log("Unable to join this channel", resp) })
        this.listenForNotifications(channel)
    },

    listenForNotifications(channel){
        channel.on('new_g_notification', payload=> {
            let msgBlock = document.createElement('li')
            msgBlock.insertAdjacentHTML('beforeend', `<a href="">${payload.notification}</a>`)
            globalFeedBox.appendChild(msgBlock)
//            globalFeedBox.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
        })
    }

} //end

GlobalNotifications.init(socket)