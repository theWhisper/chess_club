import {Socket, Presence} from "phoenix"
import socket from "./socket"

let presences ={}
let userList = document.getElementById("members-list")
let onlineCount = document.getElementById("online-members-count")
let roomFeedBox = document.getElementById('room-feed-box')
roomFeedBox.scrollIntoView({block: "end", inline: "nearest"});


const render = function(presences) {
  let onlineUsers = Presence.list(presences, (_id, {metas: [user, ...rest]}) => {
    return onlineUserTemplate(user);
  }).join("")

  document.querySelector("#members-list").innerHTML = onlineUsers;
}

const onlineUserTemplate = function(user) {
  return `
    <div id="online-user">
      <strong class="text-secondary">${user.name}</strong>
    </div>
  `
}

let Room ={
    init(socket){
        let path = window.location.pathname.split('/')
        let room = path[path.length -1]
        let channel = socket.channel('room:' + room, {})
        channel.join()
        this.listenForChats(channel)
    },

    listenForChats(channel){
        document.getElementById('room-feed-form').addEventListener('submit',function(e){
            e.preventDefault()

            let userMsg = document.getElementById('user-msg').value

            channel.push('shout', {body:userMsg})
            .receive("warning", (reply) => {
                let msgBlock = document.createElement('li')
                msgBlock.classList.add("warning-red");
                msgBlock.insertAdjacentHTML('beforeend', `${reply.from}: ${reply.body}`)
                newsFeedBox.appendChild(msgBlock)
                newsFeedBox.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});}
            )
            .receive("adminCommand", (reply) => {
                let msgBlock = document.createElement('li')
                msgBlock.classList.add("command-applied");
                msgBlock.insertAdjacentHTML('beforeend', `${reply.from}: ${reply.body}`)
                roomFeedBox.appendChild(msgBlock)
                roomFeedBox.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});}
            )
            .receive("unknown_command", (reply) => {
                console.log(reply)
                let msgBlock = document.createElement('li')
                msgBlock.classList.add("warning-red");
                msgBlock.insertAdjacentHTML('beforeend', `${reply}`)
                roomFeedBox.appendChild(msgBlock)
                roomFeedBox.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});}
            )

            document.getElementById('user-msg').value =''

        })

        channel.on('shout', payload=> {
            console.log(payload)
            let msgBlock = document.createElement('li')
            msgBlock.insertAdjacentHTML('beforeend', `<a href="">${payload.name}</a>: ${payload.body}`)
            roomFeedBox.appendChild(msgBlock)
            roomFeedBox.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
        })

        channel.on('presence_state', state => {
            presences = Presence.syncState(presences, state)
            render(presences)
            onlineCount.innerHTML = Presence.list(presences).length
        })

        channel.on('presence_diff', diff => {
            presences = Presence.syncDiff(presences, diff)
            render(presences)
            onlineCount.innerHTML = Presence.list(presences).length
        })

    }

} //end

Room.init(socket)