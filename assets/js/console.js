// Now that you are connected, you can join channels with a topic:
import {Socket, Presence} from "phoenix"
import socket from "./socket"
let channel = socket.channel('console', {})
channel.join().receive("ok", resp =>  console.log(`Welcome to Chess Club\n
    _KQ_
  _/____\\_
  \\      /
   \\____/
   \(____\)
    |  |
    |__|
   /    \\
  \(______\)
 \(________\)\n
Happy Kings Play`) ).receive("error", resp => { console.log("Unable to join this server", resp) })


let presences ={}
let userList = document.getElementById("members-list")
let onlineCount = document.getElementById("online-members-count")
let newsFeedBox = document.getElementById('news-feed-box')
newsFeedBox.scrollIntoView({block: "end", inline: "nearest"});


/* new challenge variables */
let game_link = ""
let players = 0
let min_1 = document.getElementById("1min")
let min_3 = document.getElementById("3min")
let min_5 = document.getElementById("5min")
let min_10 = document.getElementById("10min")

const render = function(presences) {
  let onlineUsers = Presence.list(presences, (_id, {metas: [user, ...rest]}) => {
    return onlineUserTemplate(user);
  }).join("")

  document.querySelector("#members-list").innerHTML = onlineUsers;
}

const onlineUserTemplate = function(user) {
  return `
    <div id="online-user">
      <strong class="text-secondary">${user.name}</strong>
    </div>
  `
}

document.getElementById('news-feed-form').addEventListener('submit',function(e){
    e.preventDefault()

    let userMsg = document.getElementById('user-msg').value

    channel.push('shout', {body:userMsg})
    .receive("warning", (reply) => {
        let msgBlock = document.createElement('li')
        msgBlock.classList.add("warning-red");
        msgBlock.insertAdjacentHTML('beforeend', `${reply.from}: ${reply.body}`)
        newsFeedBox.appendChild(msgBlock)
        newsFeedBox.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});}
    )
    .receive("adminCommand", (reply) => {
        let msgBlock = document.createElement('li')
        msgBlock.classList.add("command-applied");
        msgBlock.insertAdjacentHTML('beforeend', `${reply.from}: ${reply.body}`)
        newsFeedBox.appendChild(msgBlock)
        newsFeedBox.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});}
    )
    .receive("unknown_command", (reply) => {
        console.log(reply)
        let msgBlock = document.createElement('li')
        msgBlock.classList.add("warning-red");
        msgBlock.insertAdjacentHTML('beforeend', `${reply}`)
        newsFeedBox.appendChild(msgBlock)
        newsFeedBox.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});}
    )

    document.getElementById('user-msg').value =''
})

channel.on('shout', payload=> {
    let msgBlock = document.createElement('li')
    msgBlock.insertAdjacentHTML('beforeend', `<a href="">${payload.name}</a>: ${payload.body}`)
    newsFeedBox.appendChild(msgBlock)
    newsFeedBox.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
})

channel.on('presence_state', state => {
    presences = Presence.syncState(presences, state)
    render(presences)
    onlineCount.innerHTML = Presence.list(presences).length
})

channel.on('presence_diff', diff => {
    presences = Presence.syncDiff(presences, diff)
    render(presences)
    onlineCount.innerHTML = Presence.list(presences).length
})

channel.on('challenge_details', payload => {
    let msgBlock = document.createElement('li')
    msgBlock.insertAdjacentHTML
    ('beforeend',
    `${payload.user} is seeking to play <a href="/game/${payload.game_id}">
    <strong>Click to Accept Challenge</strong></a>
    `)
    newsFeedBox.appendChild(msgBlock)
    newsFeedBox.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
})


min_1.addEventListener("click", function(){
     channel.push('1min', {challenge:"1min"})
})