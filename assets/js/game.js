import {Socket, Presence} from "phoenix"
import socket from "./socket"
import Chess from "./chess"
import {INPUT_EVENT_TYPE, MOVE_INPUT_MODE, Chessboard} from "./Chessboard.js"

let chess = new Chess()

let presences ={}
let userList = document.getElementById("members-list")
let onlineCount = document.getElementById("online-members-count")

/* moves */
let game_pgn = document.getElementById("game-pgn")
let insertMove = document.createElement( 'div' );


let path = window.location.pathname.split('/')
let game = path[path.length -1]
let channel = socket.channel('game:' + game, {})
channel.join()
  .receive("ok", resp =>  console.log(`You will receive game updates now.`))
  .receive("error", resp => { console.log("Unable to join this game", resp) })

const render = function(presences) {
  let onlineUsers = Presence.list(presences, (_id, {metas: [user, ...rest]}) => {
    return onlineUserTemplate(user);
  }).join("")

  document.querySelector("#members-list").innerHTML = onlineUsers;
}

const onlineUserTemplate = function(user) {
  return `
    <div id="online-user">
      <strong class="text-secondary">${user.name}</strong>
    </div>
  `
}


const board = new Chessboard(document.getElementById("board"), {
    position: "start",
    moveInputMode: MOVE_INPUT_MODE.dragPiece,
    responsive: true,
    style: {
            cssClass: "default",
            showCoordinates: true,
            showBorder: true,
            aspectRatio: 0.9
        },
    sprite: {url: "../images/chessboard-sprite.svg"},
    animationDuration: 300
})

board.enableMoveInput(inputHandler)
function inputHandler(event) {
    switch (event.type) {
        case INPUT_EVENT_TYPE.moveStart:
            return true
        case INPUT_EVENT_TYPE.moveDone:
            channel.push('move', {move: `${event.squareFrom}${event.squareTo}`})
                .receive("move", (reply) => {
                    console.log(reply)
                })
            return true
        case INPUT_EVENT_TYPE.moveCanceled:
            return
    }
}

channel.on('new_g_notification', payload=> {
    console.log(payload)
})

channel.on('presence_state', state => {
        presences = Presence.syncState(presences, state)
        render(presences)
        onlineCount.innerHTML = Presence.list(presences).length
})

channel.on('presence_diff', diff => {
    presences = Presence.syncDiff(presences, diff)
    render(presences)
    onlineCount.innerHTML = Presence.list(presences).length
})

channel.on('move', payload=> {
    console.log(chess.fen())
    if( chess.move(payload.move,{sloppy: true}) != null ) {
        board.setPosition(chess.fen())
        game_pgn.innerText = parseHistory(chess.history())
        updateStatus()
    }
    else {
        board.setPosition(chess.fen())
    }
})

function parseHistory(history){
// let pgnHistory = history.reduce((a,c,i) => a + (i%2===0 ? `${(i/2|0)+1}. ${c} ` : `${c}\n`), '')
 let pgnHistory = []
 history.forEach((item, idx) => { if (idx % 2 !== 0) { pgnHistory.push(`${item}, ${history[idx -1]}`)} });
 pgnHistory.forEach((x, i) => { console.log(`${x}.`)
});
 console.log(pgnHistory)
 return pgnHistory
}


function updateStatus () {
  var status = ''

  var moveColor = 'White'
  if (chess.turn() === 'b') {
    moveColor = 'Black'
  }

  // checkmate?
  if (chess.in_checkmate()) {
    status = 'Game over, ' + moveColor + ' is in checkmate.'
  }

  // draw?
  else if (chess.in_draw()) {
    status = 'Game over, drawn position'
  }

  // game still on
  else {
    status = moveColor + ' to move'
    // check?
    if (chess.in_check()) {
      status += ', ' + moveColor + ' is in check'
    }
  }
}




