defmodule ChessClubWeb.ConsoleChannel do
  use ChessClubWeb, :channel

  alias ChessClub.ConsoleContext
  alias ChessClubWeb.Channels.Presence
  alias ChessClubWeb.Helpers.CommandParser

  @impl true
  def join("console", _payload, socket) do
    send self(), :after_join
    {:ok, socket}
  end


#   Channels can be used in a request/response fashion
#   by sending replies to elixir processes
  @impl true
  def handle_info(:after_join, socket) do
    Presence.track(socket, socket.assigns.current_user, %{
      name: socket.assigns.current_user,
    })
    push socket, "presence_state", Presence.list(socket)

    IO.inspect(socket)
    {:noreply, socket}
  end


  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  @impl true
  def handle_in("ping", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end

  # It is also common to receive messages from the client and
  # broadcast to everyone in the current topic (console:lobby).
  @impl true
  def handle_in("shout", payload, socket) do
    # Interceptor for the user messages for inappropriate words
#    Interceptor.curse_test(payload, socket)

    case CommandParser.parse(payload["body"]) do
      {:ok, command} -> apply(command)
        {:reply, {:adminCommand, %{from: "adminBOT", body: "command successfully applied!"}}, socket}
      {:text, input} ->
        payload = Map.put(payload, "name", socket.assigns.current_user)
        IO.inspect(payload)
        broadcast socket, "shout", payload
        ConsoleContext.create_console_feed(payload)
        {:noreply, socket}
      {:curse_word, _input} ->
        {:reply, {:warning, %{from: "adminBOT", body: "watch your language!"}}, socket}
      {:error, _} ->
        {:reply, {:unknown_command, %{from: "adminBOT", body: "unknown command."}}, socket}
      nil ->
        {:noreply, socket}
    end
  end


  # It is also common to receive messages from the client and
  # broadcast to everyone in the current topic (game:lobby).
  @impl true
  def handle_in("1min", payload, socket) do
    payload = %{"user": socket.assigns.current_user,
                "game_id": generate_game_id
               }
    broadcast socket, "challenge_details", payload
    {:noreply, socket}
  end

  # Add authorization logic here as required.
#  defp authorized?(_payload) do
#    true
#  end


  # Apply admin commands
  defp apply({:mute, username}) do
    IO.inspect("applying command")
  end

  # Apply admin commands
  defp apply({:ban, username}) do
    IO.inspect("applying command")
  end

  # Apply admin commands
  defp apply({:gnot, input}) do
    IO.inspect("applying command")
    notification = %{"notification" => input}
    ChessClubWeb.Endpoint.broadcast("gnotifications", "new_g_notification", notification)
  end


  defp generate_game_id() do
    game_id = Ecto.UUID.generate()
              |> String.split("-")
              |> Enum.join("")
  end

end


#if authorized?(payload) do
#  {:ok, socket}
#else
#  {:error, %{reason: "unauthorized"}}
#end