defmodule ChessClubWeb.UserSocket do
  use Phoenix.Socket

  import ChessClubWeb.Helpers.GenerateAnonymousId, only: [generate_id: 1]


  ## Channels
  #channel "room:*", ChessClubWeb.RoomChannel
  channel "console", ChessClubWeb.ConsoleChannel
  channel "gnotifications", ChessClubWeb.GlobalNotificationsChannel
  channel "room:*", ChessClubWeb.RoomChannel
  channel "game:*", ChessClubWeb.GameChannel



  # Socket params are passed from the client and can
  # be used to verify and authenticate a user. After
  # verification, you can put default assigns into
  # the socket that will be set for all channels, ie
  #
  #     {:ok, assign(socket, :user_id, verified_user_id)}
  #
  # To deny connection, return `:error`.
  #
  # See `Phoenix.Token` documentation for examples in
  # performing token verification on connect.
  @impl true
  def connect(%{"token" => token}, socket, _connect_info) do
    # max_age: 1209600 is equivalent to two weeks in seconds
    if token == "" do
      {:ok, assign(socket, :current_user, generate_id("Anonymous"))}
    else
      case Phoenix.Token.verify(socket, "user socket", token, max_age: 1209600) do
        {:ok, user_meta} ->
          {:ok, assign(socket, :current_user, user_meta)}
        {:error, _reason} ->
          :error
      end
    end

#    {:ok, socket}
  end

  # Socket id's are topics that allow you to identify all sockets for a given user:
  #
  #     def id(socket), do: "user_socket:#{socket.assigns.user_id}"
  #
  # Would allow you to broadcast a "disconnect" event and terminate
  # all active sockets and channels for a given user:
  #
  #     ChessClubWeb.Endpoint.broadcast("user_socket:#{user.id}", "disconnect", %{})
  #
  # Returning `nil` makes this socket anonymous.
  @impl true
  def id(_socket), do: nil
end
