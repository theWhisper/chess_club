defmodule ChessClubWeb.GlobalNotificationsChannel do
  use ChessClubWeb, :channel

  @impl true
  def join("gnotifications", _payload, socket) do
    {:ok, socket}
  end
end
