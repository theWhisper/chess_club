defmodule ChessClubWeb.GameChannel do
  use ChessClubWeb, :channel

  alias ChessClubWeb.Channels.Presence

#  @impl true
#  def join("game:" <> game, _payload, socket) do
#    send self(), :after_join
#    send self(), :create_game
#    {:ok, socket}
#  end

  @impl true
  def join("game:" <> game, _payload, socket) do
    if authorized?(socket) do
      send self(), :after_join
      {:ok, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  #   Channels can be used in a request/response fashion
  #   by sending replies to elixir processes
  @impl true
  def handle_info(:after_join, socket) do
    Presence.track(socket, socket.assigns.current_user, %{
      name: socket.assigns.current_user,
    })
    push socket, "presence_state", Presence.list(socket)
    {:noreply, socket}

  end

  # It is also common to receive messages from the client and
  # broadcast to everyone in the current topic (game:lobby).
  @impl true
  def handle_in("shout", payload, socket) do
    broadcast socket, "shout", payload
    {:noreply, socket}
  end

  # It is also common to receive messages from the client and
  # broadcast to everyone in the current topic (game:lobby).
  @impl true
  def handle_in("move", payload, socket) do
    move = payload["move"]
    IO.inspect(payload)
    broadcast socket, "move", payload
    {:noreply, socket}
  end

  # It is also common to receive messages from the client and
  # broadcast to everyone in the current topic (game:lobby).
  @impl true
  def handle_in("new_challenge", payload, socket) do
    broadcast socket, "move", payload
    {:noreply, socket}
  end

#  # Add authorization logic here as required.
#  defp authorized?(_payload) do
#    true
#  end

  defp authorized?(socket) do
    number_of_players(socket) < 2
  end

#  defp authorized?(socket, screen_name) do
#    number_of_players(socket) < 2 && !existing_player?(socket, screen_name)
#  end


  defp number_of_players(socket) do
    socket
    |> Presence.list()
    |> Map.keys()
    |> length()
  end

#  defp existing_player?(socket, screen_name) do
#    socket
#    |> Presence.list()
#    |> Map.has_key?(screen_name)
#  end

end
