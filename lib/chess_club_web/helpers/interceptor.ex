defmodule ChessClubWeb.Helpers.Interceptor do

	@moduledoc """
	This is the ChessClubWeb.Helpers.Interceptor module.

	The module has a collection of helper functions that are used to capture the socket incoming texts.
	"""


	@doc """
	All texts that are not commands will go through this test. We check
	if the text is a bad word defined in the regexp below.

	If it is captured then by the test then it should reply to the socket
	that sent the text with a  warning.

	If it is not captured then it will return the text atom with the text.

	## Examples

	    iex> curse_test("shit")
	    {:curse_word, {"shit"}

	    iex> curse_test("hola")
			{:text, {"hola"}

	"""
	def curse_test(input) do
		case String.match?(input,~r/shit|fuck|cunt/i) do
			true ->
				{:curse_word, {input}}
			false ->
				{:text, {input}}
		end
	end


end