defmodule ChessClubWeb.Helpers.CommandParser do

  alias ChessClubWeb.Helpers.Interceptor


  def parse(input) do
    input
    |> String.split(" ", trim: true)
    |> case do
      [":mute", username | _] ->
        {:ok, {:mute, username}}
      [":ban", username | _] ->
        {:ok, {:ban, username}}
      [":gnot" | rest] ->
        {:ok, {:gnot, Enum.join(rest, " ")}}
      other ->
        Interceptor.curse_test(input)
    end
  end
end
