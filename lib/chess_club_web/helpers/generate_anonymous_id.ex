defmodule ChessClubWeb.Helpers.GenerateAnonymousId do
  def generate_id(prefix) do
    id = System.unique_integer([:positive, :monotonic])
    id = Integer.to_string(id)
    id = prefix <> id
    id
  end
end