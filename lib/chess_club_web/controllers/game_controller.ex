defmodule ChessClubWeb.GameController do
  use ChessClubWeb, :controller


  def new(conn, _params) do
    render(conn, "new.html")
  end
end
