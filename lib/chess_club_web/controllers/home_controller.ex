defmodule ChessClubWeb.HomeController do
  use ChessClubWeb, :controller

  alias ChessClub.RoomContext

  def index(conn, _params) do
    rooms = RoomContext.list_rooms()
    render(conn, "index.html", rooms: rooms)
  end
end
