defmodule ChessClub.ConsoleContext do
  @moduledoc """
  The ConsoleContext context.
  """

  import Ecto.Query, warn: false
  alias ChessClub.Repo

  alias ChessClub.ConsoleContext.ConsoleFeed

  @doc """
  Returns the list of console_feeds.

  ## Examples

      iex> list_console_feeds()
      [%ConsoleFeed{}, ...]

  """
  def list_console_feeds do
    Repo.all(ConsoleFeed)
  end

  @doc """
  Gets a single console_feed.

  Raises `Ecto.NoResultsError` if the Console feed does not exist.

  ## Examples

      iex> get_console_feed!(123)
      %ConsoleFeed{}

      iex> get_console_feed!(456)
      ** (Ecto.NoResultsError)

  """
  def get_console_feed!(id), do: Repo.get!(ConsoleFeed, id)

  @doc """
  Creates a console_feed.

  ## Examples

      iex> create_console_feed(%{field: value})
      {:ok, %ConsoleFeed{}}

      iex> create_console_feed(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_console_feed(attrs \\ %{}) do
    %ConsoleFeed{}
    |> ConsoleFeed.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a console_feed.

  ## Examples

      iex> update_console_feed(console_feed, %{field: new_value})
      {:ok, %ConsoleFeed{}}

      iex> update_console_feed(console_feed, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_console_feed(%ConsoleFeed{} = console_feed, attrs) do
    console_feed
    |> ConsoleFeed.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a console_feed.

  ## Examples

      iex> delete_console_feed(console_feed)
      {:ok, %ConsoleFeed{}}

      iex> delete_console_feed(console_feed)
      {:error, %Ecto.Changeset{}}

  """
  def delete_console_feed(%ConsoleFeed{} = console_feed) do
    Repo.delete(console_feed)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking console_feed changes.

  ## Examples

      iex> change_console_feed(console_feed)
      %Ecto.Changeset{data: %ConsoleFeed{}}

  """
  def change_console_feed(%ConsoleFeed{} = console_feed, attrs \\ %{}) do
    ConsoleFeed.changeset(console_feed, attrs)
  end
end
