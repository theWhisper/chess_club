defmodule ChessClub.ConsoleContext.ConsoleFeed do
  use Ecto.Schema
  import Ecto.Changeset

  schema "console_feeds" do
    field :body, :string
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(console_feed, attrs) do
    console_feed
    |> cast(attrs, [:name, :body])
    |> validate_required([:name, :body])
  end
end
