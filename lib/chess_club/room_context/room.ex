defmodule ChessClub.RoomContext.Room do
  use Ecto.Schema
  import Ecto.Changeset

  schema "rooms" do
    field :description, :string
    field :name, :string
    field :slug, :string

    timestamps()
  end

  @doc false
  def changeset(room, attrs) do
    room
    |> cast(attrs, [:name, :description])
    |> validate_required([:name, :description])
    |> validate_name()
  end


  defp validate_name(changeset) do
    changeset
    |> validate_required([:name])
    |> validate_format(:name, ~r/^[\w]+([-_\s]{1}[a-z0-9]+)*$/, message: "only letters and numbers allowed")
    |> validate_length(:name, max: 160)
    |> unsafe_validate_unique(:name, ChessClub.Repo)
    |> unique_constraint(:name)
  end

end
