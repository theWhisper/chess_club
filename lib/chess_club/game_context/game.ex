defmodule ChessClub.GameContext.Game do
  use Ecto.Schema
  import Ecto.Changeset

  schema "games" do
    field :b_clock, :string
    field :b_player, :string
    field :pgn, :string
    field :result, :string
    field :type, :string
    field :w_clock, :string
    field :w_player, :string

    timestamps()
  end

  @doc false
  def changeset(game, attrs) do
    game
    |> cast(attrs, [:pgn, :w_player, :b_player, :w_clock, :b_clock, :type, :result])
    |> validate_required([:pgn, :w_player, :b_player, :w_clock, :b_clock, :type, :result])
  end
end
