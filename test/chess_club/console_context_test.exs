defmodule ChessClub.ConsoleContextTest do
  use ChessClub.DataCase

  alias ChessClub.ConsoleContext

  describe "console_feeds" do
    alias ChessClub.ConsoleContext.ConsoleFeed

    @valid_attrs %{body: "some body", name: "some name"}
    @update_attrs %{body: "some updated body", name: "some updated name"}
    @invalid_attrs %{body: nil, name: nil}

    def console_feed_fixture(attrs \\ %{}) do
      {:ok, console_feed} =
        attrs
        |> Enum.into(@valid_attrs)
        |> ConsoleContext.create_console_feed()

      console_feed
    end

    test "list_console_feeds/0 returns all console_feeds" do
      console_feed = console_feed_fixture()
      assert ConsoleContext.list_console_feeds() == [console_feed]
    end

    test "get_console_feed!/1 returns the console_feed with given id" do
      console_feed = console_feed_fixture()
      assert ConsoleContext.get_console_feed!(console_feed.id) == console_feed
    end

    test "create_console_feed/1 with valid data creates a console_feed" do
      assert {:ok, %ConsoleFeed{} = console_feed} = ConsoleContext.create_console_feed(@valid_attrs)
      assert console_feed.body == "some body"
      assert console_feed.name == "some name"
    end

    test "create_console_feed/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = ConsoleContext.create_console_feed(@invalid_attrs)
    end

    test "update_console_feed/2 with valid data updates the console_feed" do
      console_feed = console_feed_fixture()
      assert {:ok, %ConsoleFeed{} = console_feed} = ConsoleContext.update_console_feed(console_feed, @update_attrs)
      assert console_feed.body == "some updated body"
      assert console_feed.name == "some updated name"
    end

    test "update_console_feed/2 with invalid data returns error changeset" do
      console_feed = console_feed_fixture()
      assert {:error, %Ecto.Changeset{}} = ConsoleContext.update_console_feed(console_feed, @invalid_attrs)
      assert console_feed == ConsoleContext.get_console_feed!(console_feed.id)
    end

    test "delete_console_feed/1 deletes the console_feed" do
      console_feed = console_feed_fixture()
      assert {:ok, %ConsoleFeed{}} = ConsoleContext.delete_console_feed(console_feed)
      assert_raise Ecto.NoResultsError, fn -> ConsoleContext.get_console_feed!(console_feed.id) end
    end

    test "change_console_feed/1 returns a console_feed changeset" do
      console_feed = console_feed_fixture()
      assert %Ecto.Changeset{} = ConsoleContext.change_console_feed(console_feed)
    end
  end
end
