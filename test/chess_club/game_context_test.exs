defmodule ChessClub.GameContextTest do
  use ChessClub.DataCase

  alias ChessClub.GameContext

  describe "games" do
    alias ChessClub.GameContext.Game

    @valid_attrs %{b_clock: "some b_clock", b_player: "some b_player", pgn: "some pgn", result: "some result", type: "some type", w_clock: "some w_clock", w_player: "some w_player"}
    @update_attrs %{b_clock: "some updated b_clock", b_player: "some updated b_player", pgn: "some updated pgn", result: "some updated result", type: "some updated type", w_clock: "some updated w_clock", w_player: "some updated w_player"}
    @invalid_attrs %{b_clock: nil, b_player: nil, pgn: nil, result: nil, type: nil, w_clock: nil, w_player: nil}

    def game_fixture(attrs \\ %{}) do
      {:ok, game} =
        attrs
        |> Enum.into(@valid_attrs)
        |> GameContext.create_game()

      game
    end

    test "list_games/0 returns all games" do
      game = game_fixture()
      assert GameContext.list_games() == [game]
    end

    test "get_game!/1 returns the game with given id" do
      game = game_fixture()
      assert GameContext.get_game!(game.id) == game
    end

    test "create_game/1 with valid data creates a game" do
      assert {:ok, %Game{} = game} = GameContext.create_game(@valid_attrs)
      assert game.b_clock == "some b_clock"
      assert game.b_player == "some b_player"
      assert game.pgn == "some pgn"
      assert game.result == "some result"
      assert game.type == "some type"
      assert game.w_clock == "some w_clock"
      assert game.w_player == "some w_player"
    end

    test "create_game/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = GameContext.create_game(@invalid_attrs)
    end

    test "update_game/2 with valid data updates the game" do
      game = game_fixture()
      assert {:ok, %Game{} = game} = GameContext.update_game(game, @update_attrs)
      assert game.b_clock == "some updated b_clock"
      assert game.b_player == "some updated b_player"
      assert game.pgn == "some updated pgn"
      assert game.result == "some updated result"
      assert game.type == "some updated type"
      assert game.w_clock == "some updated w_clock"
      assert game.w_player == "some updated w_player"
    end

    test "update_game/2 with invalid data returns error changeset" do
      game = game_fixture()
      assert {:error, %Ecto.Changeset{}} = GameContext.update_game(game, @invalid_attrs)
      assert game == GameContext.get_game!(game.id)
    end

    test "delete_game/1 deletes the game" do
      game = game_fixture()
      assert {:ok, %Game{}} = GameContext.delete_game(game)
      assert_raise Ecto.NoResultsError, fn -> GameContext.get_game!(game.id) end
    end

    test "change_game/1 returns a game changeset" do
      game = game_fixture()
      assert %Ecto.Changeset{} = GameContext.change_game(game)
    end
  end
end
