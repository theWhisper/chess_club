defmodule ChessClub.Repo.Migrations.AddDefaultValueToConsoleFeeds do
  use Ecto.Migration

  def change do
    alter table(:console_feeds) do
      modify :type, :string, default: "text"
    end
  end
end
