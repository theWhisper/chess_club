defmodule ChessClub.Repo.Migrations.CreateGames do
  use Ecto.Migration

  def change do
    create table(:games) do
      add :pgn, :string
      add :w_player, :string
      add :b_player, :string
      add :w_clock, :string
      add :b_clock, :string
      add :type, :string
      add :result, :string

      timestamps()
    end

  end
end
