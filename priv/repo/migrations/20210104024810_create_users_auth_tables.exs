defmodule ChessClub.Repo.Migrations.CreateUsersAuthTables do
  use Ecto.Migration

  def change do
    execute "CREATE EXTENSION IF NOT EXISTS citext", ""

    create table(:users) do
      add :email, :citext, null: false
      add :hashed_password, :string, null: false
      add :confirmed_at, :naive_datetime
      add :firstname, :string
      add :lastname, :string
      add :shadow_ban, :boolean, default: false

      # Admin or less levels
      add :role, :string, default: "user"

      # GM, IM etc. FIDE titles and others.
      #Should be only two or 3 Letters. e.g GM or FIA
      add :title, :string, null: true

      timestamps()
    end

    create unique_index(:users, [:email])

    create table(:users_tokens) do
      add :user_id, references(:users, on_delete: :delete_all), null: false
      add :token, :binary, null: false
      add :context, :string, null: false
      add :sent_to, :string
      timestamps(updated_at: false)
    end

    create index(:users_tokens, [:user_id])
    create unique_index(:users_tokens, [:context, :token])
  end
end
