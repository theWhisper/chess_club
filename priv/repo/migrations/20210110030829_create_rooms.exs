defmodule ChessClub.Repo.Migrations.CreateRooms do
  use Ecto.Migration

  def change do
    create table(:rooms) do
      add :creator, :string
      add :invitation_key, :string
      add :name, :string
      add :private, :boolean, default: false
      add :slug, :string
      add :description, :text

      timestamps()
    end

    create unique_index(:rooms, [:name])


  end
end
