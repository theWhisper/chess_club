defmodule ChessClub.Repo.Migrations.CreateConsoleFeeds do
  use Ecto.Migration

  def change do
    create table(:console_feeds) do
      add :name, :string
      add :body, :string
      add :type, :string


      timestamps()
    end

  end
end
